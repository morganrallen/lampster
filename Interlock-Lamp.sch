EESchema Schematic File Version 3
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:Zilog
LIBS:zetex
LIBS:Xicor
LIBS:Worldsemi
LIBS:wiznet
LIBS:video
LIBS:ttl_ieee
LIBS:triac_thyristor
LIBS:transf
LIBS:switches
LIBS:supertex
LIBS:stm32
LIBS:stm8
LIBS:silabs
LIBS:sensors
LIBS:RFSolutions
LIBS:rfcom
LIBS:relays
LIBS:references
LIBS:pspice
LIBS:Power_Management
LIBS:powerint
LIBS:Oscillators
LIBS:onsemi
LIBS:nxp_s08
LIBS:nxp_kinetis
LIBS:nxp_armmcu
LIBS:nxp
LIBS:nordicsemi
LIBS:msp430
LIBS:motors
LIBS:motor_drivers
LIBS:modules
LIBS:microchip_pic32mcu
LIBS:microchip_pic24mcu
LIBS:microchip_pic18mcu
LIBS:microchip_pic16mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic10mcu
LIBS:microchip_dspic33dsc
LIBS:mechanical
LIBS:maxim
LIBS:logic_programmable
LIBS:LEM
LIBS:leds
LIBS:Lattice
LIBS:ir
LIBS:intersil
LIBS:infineon
LIBS:hc11
LIBS:graphic_symbols
LIBS:gennum
LIBS:ftdi
LIBS:ESD_Protection
LIBS:elec-unifil
LIBS:driver_gate
LIBS:diode
LIBS:Devices-BJH
LIBS:dc-dc
LIBS:cmos_ieee
LIBS:brooktre
LIBS:bosch
LIBS:bbd
LIBS:battery_management
LIBS:analog_devices
LIBS:Altera
LIBS:allegro
LIBS:actel
LIBS:ac-dc
LIBS:74xgxx
LIBS:Interlock-Lamp-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L LED D1
U 1 1 59CBE6DF
P 2200 2200
F 0 "D1" H 2200 2300 50  0000 C CNN
F 1 "LED" H 2200 2100 50  0000 C CNN
F 2 "LEDs:LED_0805_HandSoldering" H 2200 2200 50  0001 C CNN
F 3 "" H 2200 2200 50  0001 C CNN
	1    2200 2200
	0    -1   -1   0   
$EndComp
$Comp
L Battery_Cell BT1
U 1 1 59CBE70A
P 7150 2550
F 0 "BT1" H 7250 2650 50  0000 L CNN
F 1 "Battery_Cell" H 7250 2550 50  0000 L CNN
F 2 "Battery_Holders:Keystone_3034_1x20mm-CoinCell" V 7150 2610 50  0001 C CNN
F 3 "" V 7150 2610 50  0001 C CNN
	1    7150 2550
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 59CBE7B2
P 5200 1750
F 0 "R1" V 5280 1750 50  0000 C CNN
F 1 "R" V 5200 1750 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 5130 1750 50  0001 C CNN
F 3 "" H 5200 1750 50  0001 C CNN
	1    5200 1750
	0    1    1    0   
$EndComp
$Comp
L CONN_01X02 J5
U 1 1 59CBECA5
P 7100 2100
F 0 "J5" H 7100 2250 50  0000 C CNN
F 1 "B1OUT" V 7200 2100 50  0000 C CNN
F 2 "Connectors:GS2" H 7100 2100 50  0001 C CNN
F 3 "" H 7100 2100 50  0001 C CNN
	1    7100 2100
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X02 J3
U 1 1 59CBED4A
P 3800 2100
F 0 "J3" H 3800 2250 50  0000 C CNN
F 1 "B2OUT" V 3900 2100 50  0000 C CNN
F 2 "Connectors:GS2" H 3800 2100 50  0001 C CNN
F 3 "" H 3800 2100 50  0001 C CNN
	1    3800 2100
	-1   0    0    -1  
$EndComp
$Comp
L CONN_01X02 J4
U 1 1 59CBEDD1
P 5700 2100
F 0 "J4" H 5700 2250 50  0000 C CNN
F 1 "B2IN" V 5800 2100 50  0000 C CNN
F 2 "Connectors:GS2" H 5700 2100 50  0001 C CNN
F 3 "" H 5700 2100 50  0001 C CNN
	1    5700 2100
	-1   0    0    -1  
$EndComp
$Comp
L CONN_01X02 J7
U 1 1 59CFF3B2
P 5700 1800
F 0 "J7" H 5700 1950 50  0000 C CNN
F 1 "B2IN2" V 5800 1800 50  0000 C CNN
F 2 "Connectors:GS2" H 5700 1800 50  0001 C CNN
F 3 "" H 5700 1800 50  0001 C CNN
	1    5700 1800
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X02 J8
U 1 1 59CFF3E2
P 7100 1800
F 0 "J8" H 7100 1950 50  0000 C CNN
F 1 "B1OUT2" V 7200 1800 50  0000 C CNN
F 2 "Connectors:GS2" H 7100 1800 50  0001 C CNN
F 3 "" H 7100 1800 50  0001 C CNN
	1    7100 1800
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X02 J6
U 1 1 59CFF410
P 3800 1800
F 0 "J6" H 3800 1950 50  0000 C CNN
F 1 "B2OUT2" V 3900 1800 50  0000 C CNN
F 2 "Connectors:GS2" H 3800 1800 50  0001 C CNN
F 3 "" H 3800 1800 50  0001 C CNN
	1    3800 1800
	-1   0    0    -1  
$EndComp
$Comp
L CONN_01X02 J1
U 1 1 59CFF43C
P 3750 2550
F 0 "J1" H 3750 2700 50  0000 C CNN
F 1 "B3IN2" V 3850 2550 50  0000 C CNN
F 2 "Connectors:GS2" H 3750 2550 50  0001 C CNN
F 3 "" H 3750 2550 50  0001 C CNN
	1    3750 2550
	1    0    0    -1  
$EndComp
$Comp
L LED D2
U 1 1 59CFF60D
P 1900 2200
F 0 "D2" H 1900 2300 50  0000 C CNN
F 1 "LED" H 1900 2100 50  0000 C CNN
F 2 "LEDs:LED_0805_HandSoldering" H 1900 2200 50  0001 C CNN
F 3 "" H 1900 2200 50  0001 C CNN
	1    1900 2200
	0    -1   -1   0   
$EndComp
Text GLabel 7000 2350 0    60   Input ~ 0
BAT+
Text GLabel 6500 2850 0    60   Input ~ 0
BAT-
Text GLabel 6900 1750 0    60   Input ~ 0
BAT+
Text GLabel 6900 1850 0    60   Input ~ 0
BAT-
Text GLabel 6900 2050 0    60   Input ~ 0
BAT+
Text GLabel 6900 2150 0    60   Input ~ 0
BAT-
Text GLabel 5450 1600 1    60   Input ~ 0
BAT+
Text GLabel 5300 2150 0    60   Input ~ 0
BAT-
Text GLabel 5900 2050 2    60   Input ~ 0
BAT+
Text GLabel 5900 2150 2    60   Input ~ 0
BAT-
Text GLabel 4900 1750 0    60   Input ~ 0
B2OUT+
Text GLabel 4000 1850 2    60   Input ~ 0
BAT-
Text GLabel 4000 2150 2    60   Input ~ 0
BAT-
Text GLabel 4000 1750 2    60   Input ~ 0
B2OUT+
Text GLabel 4000 2050 2    60   Input ~ 0
B2OUT+
$Comp
L CONN_01X02 J9
U 1 1 59D00D54
P 3200 2100
F 0 "J9" H 3200 2250 50  0000 C CNN
F 1 "B3IN" V 3300 2100 50  0000 C CNN
F 2 "Connectors:GS2" H 3200 2100 50  0001 C CNN
F 3 "" H 3200 2100 50  0001 C CNN
	1    3200 2100
	1    0    0    -1  
$EndComp
Text GLabel 2900 2400 3    60   Input ~ 0
BAT-
Text GLabel 2900 1900 1    60   Input ~ 0
B2OUT+
Text GLabel 3550 2500 0    60   Input ~ 0
B2OUT+
$Comp
L SW_SPDT SW1
U 1 1 59D02C38
P 6800 2650
F 0 "SW1" H 6800 2325 50  0000 C CNN
F 1 "SW_SPDT" H 6800 2416 50  0000 C CNN
F 2 "Buttons_Switches_SMD:JS202011SCQN" H 6800 2650 50  0001 C CNN
F 3 "" H 6800 2650 50  0001 C CNN
	1    6800 2650
	-1   0    0    1   
$EndComp
$Comp
L CONN_01X02 J2
U 1 1 59EEDA58
P 6950 3200
F 0 "J2" H 6950 3350 50  0000 C CNN
F 1 "PWR_BRIDGE" V 7050 3200 50  0000 C CNN
F 2 "AnyColorButton:GS2-no-mask" H 6950 3200 50  0001 C CNN
F 3 "" H 6950 3200 50  0001 C CNN
	1    6950 3200
	0    -1   1    0   
$EndComp
Wire Wire Line
	7000 2350 7150 2350
Wire Wire Line
	7000 2650 7150 2650
Wire Wire Line
	5500 1750 5350 1750
Wire Wire Line
	5450 1600 5450 1750
Connection ~ 5450 1750
Wire Wire Line
	5450 1850 5450 2150
Wire Wire Line
	4900 1750 5050 1750
Wire Wire Line
	5450 1850 5500 1850
Wire Wire Line
	1900 2050 3000 2050
Wire Wire Line
	2900 1900 2900 2050
Connection ~ 2900 2050
Wire Wire Line
	2900 2400 2900 2150
Wire Wire Line
	2900 2150 3000 2150
Wire Wire Line
	1900 2350 2900 2350
Connection ~ 2900 2350
Wire Wire Line
	3550 2600 3000 2600
Wire Wire Line
	3000 2600 3000 2250
Wire Wire Line
	3000 2250 2900 2250
Connection ~ 2900 2250
Wire Wire Line
	6500 2850 6500 2750
Wire Wire Line
	6500 2750 6600 2750
Wire Wire Line
	5450 2150 5300 2150
Connection ~ 2200 2350
Connection ~ 2200 2050
Wire Wire Line
	6900 3000 6900 2850
Wire Wire Line
	6900 2850 6550 2850
Wire Wire Line
	6550 2850 6550 2750
Connection ~ 6550 2750
Wire Wire Line
	7150 2650 7150 2900
Wire Wire Line
	7150 2900 7000 2900
Wire Wire Line
	7000 2900 7000 3000
$EndSCHEMATC
