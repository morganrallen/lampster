$fn = 100;

3D = true;
FCu = true;
FSilk = !3D;
BSilk = !3D;

// output units are mm when exporting as DXF
MIL_IN_MM = 39.370078740157;

board_thickness = 63; // 1.6002mm
board_thickness_mm = board_thickness / MIL_IN_MM;
bt_mm = board_thickness_mm;
margin = 10; // 0.254mm
margin_mm = margin / MIL_IN_MM;

slot_width = bt_mm + margin_mm; // 1.6002mm + 0.254mm= 1.8542mm

tab = bt_mm * 3;
tab_inset = tab + margin_mm;

arc_height = 20;
arc_width = 20;

if(3D) {
  rotate([ 90, 0, 0 ])
  linear_extrude(board_thickness_mm)
    arm_board();
  
  rotate([ 0, 0, 90 ])
  translate([ -13.2, 5.4, -2 ])
  linear_extrude(board_thickness_mm)
    batt_board();

  translate([ 2.4, -.85, 15.3 ])
  linear_extrude(board_thickness_mm)
    led_board();
} else { 
  arm_board();
  
  rotate([ 0, 0, 90 ])
    translate([ -21, -3 ])
      batt_board();

  translate([-6, 11])
    led_board();
}

// arm board
module arm_board() {
  board_arc(0, 0, arc_height, arc_width);
  
  // top
  translate([ 0, arc_height - tab ])
    square([ tab, tab * 2]);
  
  // bottom
  translate([ -(arc_width + tab), -(bt_mm * 1.25)])
    square( [ tab, bt_mm *1.25 ] );
}

module led_board() {
  difference() {
    circle(arc_height / 3);
    slot(0, 0, slot_width, tab_inset);
  }
}


module batt_board() {
  if(FCu) {
    difference() {
      square([ 24.8, 22 ]);
      slot(12.4, 17 , tab_inset, slot_width);
    }
  }
  
  if(FSilk) {
    rotate([ 0, 0, -90 ])
    translate([ -20, 1])
    scale([ 2, 2, 0 ])
    import("./OSHW-Logo-R2000.dxf");
  }
}

module board_arc(x, y, height, width) {
  difference() {
    arc(x, y, height + tab, width + tab);
    arc(x, y, height, width);
  }
}


module arc(x, y, height, width) {
  translate([ x, y ])
  difference(){
    circle(height,width);
    square([ height,width ]);
  
    translate([ 0, -height ])
      square([ height, width ]);
    
    translate([ -width, -height ])
      square([ height, width ]);
  }
}

//slot(4, 5, 10, 10);
  
module slot(x, y, width, length) {
  translate([ x - (length / 2), y - (width / 2) ])
    square([ length, width ]);
}
